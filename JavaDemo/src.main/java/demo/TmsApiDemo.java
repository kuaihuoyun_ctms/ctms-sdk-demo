package demo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import util.HttpClientHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * TMS接口调用实例，以 "获取登陆凭证接口"、 "创建订单接口"、 "批量查询订单接口" 为例
 * 其余接口调用方式相同；
 * 以下代码仅做基本的调用演示，逻辑并不严密，请客户方根据需求自行完善；
 * 参数只传了必填参数和部分非必填参数，完整的参数请查看文档
 */
public class TmsApiDemo {
    //测试环境
    private static final String URL = "http://api.test.56ctms.com";

    private static final String USER_NAME = "19012345678";

    private static final String PASSWORD = "123456";

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {
        System.out.println("*********************获取登陆凭证接口*********************");
        JSONObject tokenParams = new JSONObject();
        tokenParams.put("userName", USER_NAME);
        tokenParams.put("password", PASSWORD);
        String result = HttpClientHelper.httpPost(URL + "/user/generate_access_token/v2", tokenParams);
        JSONObject tokenResponse = JSONObject.parseObject(result);
        JSONObject tokenInfo = tokenResponse.getJSONObject("data");
        //token，后续请求需要用到，获取一次即可，有效期1年
        String accessToken = tokenInfo.getString("accessToken");
        //刷新token，用于刷新token
        String refreshToken = tokenInfo.getString("refreshToken");
        System.out.println("ACCESS_TOKEN = " + accessToken);
        System.out.println("REFRESH_TOKEN = " + refreshToken);
        System.out.println();

        System.out.println("*********************创建订单接口*********************");
        JSONObject createOrderParams = new JSONObject();
        JSONArray orders = new JSONArray();
        createOrderParams.put("orderList", orders);
        JSONObject order = new JSONObject();
        //用于唯一性检测，此字段不在系统中展示:必填
        order.put("orderNumber", "KHY_TEST_123456");
        //发货人地址:必填
        order.put("consignerAddress", "浙江省杭州市西湖区计量大厦");
        //发货人姓名:必填
        order.put("consignerName", "快货运测试");
        //发货人电话:必填
        order.put("consignerPhone", "19012345678");
        //提货时间:时间戳,精确到秒:必填
        order.put("deliveryTime", System.currentTimeMillis() / 1000);
        //提送类型:选填, 1)自提，2)送货
        order.put("deliveryType", 1);
        //客户单号:选填
        order.put("customerOrderNumber", "KHY_TEST_123456");
        //预约送到时间:时间戳,精确到秒:选填
        order.put("appointArriveTime", System.currentTimeMillis() / 1000);
        //收货人地址:可不填
        order.put("consigneeAddress", "xxx");
        //收货人姓名:可不填
        order.put("consigneeName", "xxx");
        //收货人电话:可不填
        order.put("consigneePhone", "19012345679");
        orders.add(order);
        result = HttpClientHelper.httpPost(URL + "/order/create_order?access_token=" + accessToken, createOrderParams);
        JSONObject createOrderResponse = JSONObject.parseObject(result);
        //TMS系统生成的订单号
        JSONArray orderNumberList = createOrderResponse.getJSONArray("data");
        System.out.println("TMS生成的单号：" + orderNumberList);
        System.out.println();

        System.out.println("*********************批量查询订单接口*********************");
        JSONObject queryOrderParams = new JSONObject();
        //页码，1开始
        queryOrderParams.put("page", 1);
        //单页最大订单数，最大50
        queryOrderParams.put("size", 3);
        //订单状态，0已撤销，1已下单，2已接单，3已装货，4已签收，1000待处理，无法查看全部状态，不传则默认0已撤销
        queryOrderParams.put("state", 1000);
        //查询时间类型，created录入时间，deliveryed提货时间，appointArrived预约提货时间，signed签收时间
        queryOrderParams.put("timeType", "created");
        queryOrderParams.put("startDate", dateFormat.format(new Date(System.currentTimeMillis() - 24 * 3600 * 1000)));
        queryOrderParams.put("endDate", dateFormat.format(new Date()));
        result = HttpClientHelper.httpPost(URL + "/order/query_orders?access_token=" + accessToken, queryOrderParams);
        JSONObject queryOrderResponse = JSONObject.parseObject(result);
        JSONObject queryOrders = queryOrderResponse.getJSONObject("data");
        //总数据量
        Integer total = queryOrders.getInteger("total");
        //页数
        Integer page = queryOrders.getInteger("page");
        //分页大小
        Integer size = queryOrders.getInteger("size");
        //总页数
        Integer totalPagesCount = queryOrders.getInteger("totalPagesCount");
        //订单信息
        JSONArray orderList = queryOrders.getJSONArray("elements");
        System.out.println(orderList);
    }


}
