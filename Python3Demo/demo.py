"""
TMS接口python demo，以下代码在python=3.7测试通过，接口调用封装在ctms_api.py中
调用接口的参数并非全部参数，完整参数请查看TMS接口文档
此demo只做调用演示用，较为简陋，对接时请根据实际业务需要完善相关逻辑
"""
from Python3Demo.ctms_api import CtmsApi
import time

# token的获取在CtmsApi中完成，请求地址使用的测试环境的地址，此处注意url结尾不要带/
api = CtmsApi(user='19012345678', password='123456', url='http://api.test.56ctms.com')


def stamp2date(stamp):
    """
    时间戳转日期
    :param stamp: 时间戳
    :return: 日期，格式：%Y-%m-%d %H:%M:%S
    """
    if not stamp:
        return None
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(stamp))


print('******************创建订单接口******************')
create_order_params = {
    'orderList': [{
        # 用于唯一性检测，此字段不在系统中展示:必填
        "orderNumber": "KHY_TEST_654321",
        # 发货人地址:必填
        "consignerAddress": "浙江省杭州市西湖区计量大厦",
        # 发货人姓名:必填
        "consignerName": "快货运测试",
        # 发货人电话:必填
        "consignerPhone": "19012345678",
        # 提货时间:时间戳,精确到秒:必填
        "deliveryTime": int(time.time()),
        # 提送类型:选填, 1)自提，2)送货
        "deliveryType": 1,
        # 客户单号:选填
        "customerOrderNumber": "KHY_TEST_654321",
        # 预约送到时间:时间戳,精确到秒:选填
        "appointArriveTime": int(time.time()),
        # 收货人地址:可不填
        "consigneeAddress": "xxx",
        # 收货人姓名:可不填
        "consigneeName": "xxx",
        # 收货人电话:可不填
        "consigneePhone": "19012345679",
    }]
}
result = api(path='/order/create_order', body=create_order_params)
print(result)

print('******************批量查询订单接口******************')
query_order_params = {
    'page': 1,
    'size': 2,
    'state': 1000,
    'timeType': 'created',
    'startDate': stamp2date(time.time() - 24 * 3600),
    'endDate': stamp2date(time.time()),
}
result = api(path='/order/query_orders', body=query_order_params)
print(result)
